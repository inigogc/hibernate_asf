package es.deusto.asf.hibernate;

import es.deusto.asf.hibernate.dao.FacultyDAO;
import es.deusto.asf.hibernate.dao.GradeDAO;
import es.deusto.asf.hibernate.model.Faculty;
import es.deusto.asf.hibernate.model.Grade;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        FacultyDAO facultyDAO = (FacultyDAO) applicationContext.getBean("facultyDAO");
        GradeDAO gradeDAO = (GradeDAO) applicationContext.getBean("gradeDAO");

        List<Faculty> facultyList = facultyDAO.findAllFaculties();
        for( Faculty faculty : facultyList )
            System.out.println("Facultad: " + faculty.getName() + ", " + faculty.getId() + ", " + faculty.getUrl());

        Faculty faculty = facultyDAO.findFacultyById("F01");
        System.out.println("Faculty F01 is: " + faculty.getName());

        List<Grade> gradeList = gradeDAO.findGradesByFaculty(faculty);
        for( Grade grade : gradeList )
            System.out.println("\tGrade: " + grade.getId() + ", " + grade.getName() + ", " + grade.getEcts());
    }
}
