package es.deusto.asf.hibernate.model;

import javax.persistence.*;

@Entity
@Table(name = "grades")
public class Grade {

	@Id
	private String id;
	private String name;
	private int ects;

	@ManyToOne
	@JoinColumn(name = "FACULTY")
	private Faculty faculty;

	public Grade() {
	}

	public Grade(String id, String name, int ects) {
		super();
		this.id = id;
		this.name = name;
		this.ects = ects;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEcts() {
		return ects;
	}
	public void setEcts(int ects) {
		this.ects = ects;
	}
	public Faculty getFaculty() {
		return faculty;
	}
	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}
}
