package es.deusto.asf.hibernate.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "faculties")
public class Faculty {

	@Id
	private String id;
	private String name;
	private String url;
	@OneToMany(mappedBy = "faculty",
		cascade = CascadeType.ALL,
		fetch = FetchType.EAGER)
	private Set<Grade> grades;
	
	public Faculty() {
	}

	public Faculty(String id, String name, String url) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Set<Grade> getGrades() {
		return grades;
	}
	public void setGrades(Set<Grade> grades) {
		this.grades = grades;
	}
}
