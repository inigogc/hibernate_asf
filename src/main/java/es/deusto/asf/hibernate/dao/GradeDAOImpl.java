package es.deusto.asf.hibernate.dao;

import es.deusto.asf.hibernate.model.Faculty;
import es.deusto.asf.hibernate.model.Grade;

import java.util.List;
import java.util.Vector;

public class GradeDAOImpl implements GradeDAO {

    @Override
    public List<Grade> findGradesByFaculty(Faculty faculty) {
        return new Vector<>(faculty.getGrades());
    }
}
