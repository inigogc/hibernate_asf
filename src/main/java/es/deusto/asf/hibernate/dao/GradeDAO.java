package es.deusto.asf.hibernate.dao;

import es.deusto.asf.hibernate.model.Faculty;
import es.deusto.asf.hibernate.model.Grade;

import java.util.List;

public interface GradeDAO {
	public List<Grade> findGradesByFaculty(Faculty faculty);
}
