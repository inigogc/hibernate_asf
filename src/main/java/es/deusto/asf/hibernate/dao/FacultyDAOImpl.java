package es.deusto.asf.hibernate.dao;

import es.deusto.asf.hibernate.model.Faculty;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class FacultyDAOImpl implements FacultyDAO {
    private SessionFactory sessionFactory;

    public FacultyDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Faculty> findAllFaculties() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Faculty> criteriaQuery = criteriaBuilder.createQuery(Faculty.class);
        criteriaQuery.from(Faculty.class);
        List<Faculty> returnedFaculties = session.createQuery(criteriaQuery).getResultList();

        transaction.commit();
        session.close();

        return returnedFaculties;
    }

    @Override
    public Faculty findFacultyById(String id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Faculty faculty = session.get(Faculty.class, id);
        transaction.commit();
        session.close();

        return faculty;
    }
}
