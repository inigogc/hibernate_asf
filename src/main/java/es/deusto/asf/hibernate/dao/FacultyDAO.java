package es.deusto.asf.hibernate.dao;

import es.deusto.asf.hibernate.model.Faculty;

import java.util.List;

public interface FacultyDAO {
	public List<Faculty> findAllFaculties();
	public Faculty findFacultyById(String id);
}
